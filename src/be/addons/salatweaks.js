'use strict';

exports.engineVersion = '2.6';


var db = require('../db');
var boards = db.boards();


const settingsHandler = require('../settingsHandler');
const uploadHandler = require('../engine/uploadHandler.js');
const gridFsHandler = require('../engine/gridFsHandler.js');
const templateHandler = require("../engine/templateHandler.js");
const domCommon = require('../engine/domManipulator').common;
const postingContent = require('../engine/domManipulator/postingContent');

const board = require("../engine/generator/board.js");

const logger = require('../logger');

const util = require('util');
const execPromisified = util.promisify(require('child_process').exec);
const exec = require('child_process').exec;

var kernel = require('../kernel');
var native = kernel.native;

var thumbSize;
var verbose;
var settings;




exports.loadSettings = function () {
	settings = settingsHandler.getGeneralSettings();
	thumbSize = settings.thumbSize;
};


// Allow files to be requested by alias filename appended at end. 
const outputFileOrig = gridFsHandler.outputFile;
gridFsHandler.outputFile = function (file, req, res, callback) {
	// /.media/t_0d674d21bb7c2c66886d65484dfc5610456ed5fec033dd88b928ca49839615ad
	// /.media/d940ddb4a99b863e8bf64f14b6cd53c4fa49bcc3a327eecd3a844162444c8ac1.png
	// /.media/t_d940ddb4a99b863e8bf64f14b6cd53c4fa49bcc3a327eecd3a844162444c8ac1.png/1611195323131.png
	if (file.startsWith("/.media/")) {
		const realNameEnd = file.indexOf("/", 8)
		if (realNameEnd !== -1) {
			file = file.substr(0, file.indexOf("/", realNameEnd))
		}
	}
	return outputFileOrig(file, req, res, callback);
}



function generateImageThumb(identifier, file, callback) {

	if (file.mime !== "image/gif" || file.size >= 1024 * 512) {
		return generateStaticThumb(identifier, file, callback);
	}

	if (file.width > 130 || file.height > 130) {
		return generateStaticThumb(identifier, file, callback);
	}

	const cmdHasAlhpa = `identify -format '%A'  ${file.pathInDisk}`;
	const cmdIsAnimated = `identify -format '%n' ${file.pathInDisk}`;

	const promises = [
		execPromisified(cmdHasAlhpa),
		execPromisified(cmdIsAnimated),
	];

	try {
		Promise.all(promises).then((results) => {


			//const hasAlpha = results[0].stdout.indexOf("True") > -1 || results[0].stdout.indexOf("Blend") > -1;

			const alphaFrames = (results[0].stdout.match(/True/g) || []).length;
			const notAlphaFrames = (results[0].stdout.match(/False/g) || []).length;

			const alphaEnough = alphaFrames >= notAlphaFrames;

			const isAnimated = parseInt((results[1].stdout)) > 1;

			/*
			console.info(results[0])
			console.info(results[1]);
			console.info(alphaEnough, isAnimated);
			*/

			if (alphaEnough && isAnimated) {
				return generateGifsicleThumb(identifier, file, callback);
			} else {
				return generateStaticThumb(identifier, file, callback, isAnimated);
			}
		});
	} catch (e) {
		console.info("Failed to check if should be animated or failed to generate gif thumb")
		console.error(e);
		return generateStaticThumb(identifier, file, callback);
	}
}


function generateGifsicleThumb(identifier, file, callback, ) {

	var thumbDestination = file.pathInDisk + '_t.gif';

	file.thumbOnDisk = thumbDestination;
	file.thumbMime = "image/gif"
	file.thumbPath = '/.media/t_' + identifier;

	const command = `gifsicle --resize-fit-height ${thumbSize} -i ${file.pathInDisk} -o ${thumbDestination}`;

	exec(command, {
		maxBuffer: Infinity
	}, function resized(error) {
		if (error) {
			console.info(error);
			callback(error);
		} else {
			return uploadHandler.transferThumbToGfs(identifier, file, callback);
		}
	});
}

function generateStaticThumb(identifier, file, callback, isAnimated = false) {

	var thumbDestination = file.pathInDisk + +'_t';

	const thumbCb = function (error) {
		if (error) {
			console.info(error);
			return callback(error);
		}
		file.thumbOnDisk = thumbDestination;
		file.thumbMime = settings.thumbExtension ? logger.getMime(thumbDestination) :
			file.mime;
		file.thumbPath = '/.media/t_' + identifier;

		uploadHandler.transferThumbToGfs(identifier, file, callback);
	};

	if (settings.thumbExtension) {
		thumbDestination += '.' + settings.thumbExtension;
	}

	if (native && file.mime !== 'image/webp' && file.mime !== 'image/gif') {
		return native.imageThumb(file.pathInDisk, thumbDestination, settings.thumbSize, thumbCb);
	}

	let command = `convert -thumbnail x${thumbSize} -background transparent -flatten -strip `;
	command += `${file.pathInDisk}[0] ${thumbDestination}`;

	exec(command, {
		maxBuffer: Infinity
	}, function resized(error) {
		thumbCb(error);
	});
}


const domStatic  = require("../engine/domManipulator/static.js")
domStatic.getTopBoards = function(boards, language) {  

	let groups = [... new Set(boards.map((it) => it.listingGroup || 0))];
	let groupedBoards = {};

	boards.forEach((it) => {
		const boardGroup = it.listingGroup || 0;
		if (!groupedBoards[boardGroup]) groupedBoards[boardGroup] = [];
		groupedBoards[boardGroup].push(it);
	});

	let boardsHtml = '';

	groups.forEach((group) => {
		boardsHtml +=  "<span class='board-group'>" + groupedBoards[group].map((it) => {
			const boardName = domCommon.clean(it.boardName);
			const boardUri = domCommon.clean(it.boardUri)
			return `<a href='/${boardUri}/'>/${boardUri}/ - ${boardName}</a>`}
		).join(" ").trim() + "</span>&nbsp;<wbr>";
	});

	return boardsHtml;
  };


function setNavLinksForPages() {
	let boardsHtml = null;

	function getBoardsLinkForNavi(callback) {
		if (boardsHtml) return callback(boardsHtml);
		boards.find({}).sort(
				{ 
					"listingGroup" : 1,
					"listingOrder" : 1
				}
			).toArray(function(err, res) {
			if (err) throw new Error(err);
			//console.log(res);

			let groups = [... new Set(res.map((it) => it.listingGroup || 0))];
			let groupedBoards = {};

			res.forEach((it) => {
				const boardGroup = it.listingGroup || 0;
				if (!groupedBoards[boardGroup]) groupedBoards[boardGroup] = [];
				groupedBoards[boardGroup].push(it);
			});

			boardsHtml = '';

			groups.forEach((group) => {

				//const boardName = 

				boardsHtml +=  "[ <span class='board-group'>" + groupedBoards[group].map((it) => 
						`<a title='${domCommon.clean(it.boardName)}' href='/${it.boardUri}/'>${it.boardUri}</a>`)
					.join(" / ").trim() + " ]</span>&nbsp;<wbr>";
			});

			callback(boardsHtml);
		});
	}

	const loadPagesOrig = templateHandler.loadPages;
	templateHandler.loadPages = function(errors, fePath, templateSettings, prebuiltObject) {
		getBoardsLinkForNavi(() => {
			return loadPagesOrig(errors, fePath, templateSettings, prebuiltObject);
		});
	};

	const checkMainChildrenOrig = templateHandler.checkMainChildren;
	templateHandler.checkMainChildren =  function(page, document, map) {

		const res = checkMainChildrenOrig(page, document, map);

		function setNavLinks(navLink) {
			const boardsLink = navLink.childNodes.find((it) => {
				return it.attrs && it.attrs.find((attr) => attr.name == "id" && attr.value == "topNaviBoards" )
			});
			if (boardsLink) {
				getBoardsLinkForNavi(function gotLink(link) {
					templateHandler.setInner(boardsLink, link)
				})
			}
		}

		const navLink1 = map["navLinkSpan"];
		const navLink2 =  map["navLinkSpan2"];

		if (navLink1) setNavLinks(navLink1);
		if (navLink2) setNavLinks(navLink2);

		return res;
	}
}

const fs = require("fs");
const postingOpsCommon = require("../engine/postingOps").common;
const { getImageBounds } = require("../engine/uploadHandler");

//uploadHandler.getImageBounds

exports.setSmileys = function() {

	const path = "../fe/static/images/smileys";
	let smileys = [];

	fs.readdir(path, function gotFiles(err, files) {
		if (err) {
			console.error("Failed to load smileys");
			throw new Error(err);
		}

		files.forEach((fileName) => {

			if (!fileName.endsWith(".gif")) return;
			
			const file = {
				pathInDisk: path + "/" + fileName
			}

			getImageBounds(file, function gotBounds(err, width, height) {
				if (err) {
					console.info("Failed to get bounds from smiley " + fileName);
					console.info(error);
					return;
				}

				const str =  fileName.substr(0, fileName.length - 4);
				const re = new RegExp(`:${str}:`, "gm");

				smileys.push({
					name: str,
					regex: re,
					width: width,
					height: height,
				});

			});
		});
	});


	const smileysUri = "/.static/images/smileys";

	const setFlagsOrig = domCommon.setFlags;
	domCommon.setFlags = function(document, flagData, language, removable) {
			const smileyLinks = smileys.map((it) => {
				const img = `<img src='${smileysUri}/${it.name}.gif' loading='lazy' width='${it.width}' height='${it.height}'>`;
				return `<a class='hymiövalita' data-hymiö='${it.name}'>${img}</a>`
			}).join(" ");
			document = document.replace('__divSmileyList_inner__', smileyLinks);
			return setFlagsOrig(document, flagData, language, removable);
	}
c
	function addSmileysToString(string, max = 0) {
		smileys.forEach((smiley) => {
			string = string.replace(smiley.regex,
				`<img class='hymiö' src='${smileysUri}/${smiley.name}.gif' width='${smiley.width}' height='${smiley.height}' alt=':${smiley.name}:'>`
			);
		});
		return string;
	}

	//const setSharedHideableElementsOrig = postingContent.setSharedHideableElements;
	postingContent.setSharedHideableElements = function(posting, removable, postingCell, preview, language) {

		postingCell = postingContent.setLabels(postingContent.setPostingFlag(posting, postingCell,
			removable), posting, language, removable, preview);
	  
		if (posting.subject) {
			postingCell = postingCell.replace('__labelSubject_location__', removable.labelSubject)
				.replace('__labelSubject_inner__', addSmileysToString(domCommon.clean(posting.subject)));
	
		} else {
			postingCell = postingCell.replace('__labelSubject_location__', '');
		}

		if (posting.id) {
			return postingCell.replace('__spanId_location__', removable.spanId)
				.replace('__labelId_inner__', posting.id).replace('__labelId_style__',
					'background-color: #' + posting.id);
		  } else {
			return postingCell.replace('__spanId_location__', '');
		  }
	}

	const getSubChunkMarkdownOrig = postingOpsCommon.getSubChunkMarkdown;
	postingOpsCommon.getSubChunkMarkdown = function(message, boards) {
		message = getSubChunkMarkdownOrig(message, boards);
		message = addSmileysToString(message);
		return message;
	}
}


postingContent.formatFileSize = function(size, language) {

	if (size === Infinity) {
	  return lang(language).guiUnlimited;
	}
  
	var orderIndex = 0;
  
	while (orderIndex < postingContent.sizeOrders.length - 1 && size > 1023) {
  
	  orderIndex++;
	  size /= 1024;
  
	}
  
	return size.toFixed(2) + ' asd ' + postingContent.sizeOrders[orderIndex];
};

exports.init = function () {
	uploadHandler.generateImageThumb = generateImageThumb;
	uploadHandler.generateGifThumb = generateGifsicleThumb;
	uploadHandler.thumbAudioMimes = ['audio/mpeg', 'audio/ogg'];
	setNavLinksForPages();


	const boardProjectionOrig = board.boardProjection;
	board.boardProjection = Object.assign(boardProjectionOrig, {order: 1});

	exports.setSmileys();
}